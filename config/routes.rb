Rails.application.routes.draw do
  scope '/api' do
    scope '/v1' do
      scope '/servers' do
        post '/' => 'api_servers#create'
        scope '/:hostname' do
          post '/cpu' => 'api_servers#store_cpu'
          post '/disk' => 'api_servers#store_disk'
          post '/process' => 'api_servers#store_process'
        end
      end
    end
  end
end
