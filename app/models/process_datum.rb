class ProcessDatum < ActiveRecord::Base
  belongs_to :server_process_datum, inverse_of: :process_data

  validates :server_process_datum, presence: true
  validates :process_cpu_usage, presence: true
  validates :process_name, presence: true

  def to_json(*a)
    { server: server_process_datum.server.hostname, cpu_usage: process_cpu_usage, created_at: server_process_datum.created_at, process_name: process_name }.to_json(*a)
  end
end
