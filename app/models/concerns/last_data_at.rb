# Updates the last_data_send_at field of the included model.

module LastDataAt
  extend ActiveSupport::Concern

  included do
    after_create :set_last_data_sent_at
  end

  protected

  def set_last_data_sent_at
    server.update_attribute(:last_data_sent_at, created_at)
  end
end