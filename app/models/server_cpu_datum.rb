class ServerCpuDatum < ActiveRecord::Base
  include LastDataAt

  belongs_to :server, inverse_of: :cpu_data

  validates :server, presence: true
  validates :cpu_usage, presence: true

  def to_json(*a)
    { server: server.hostname, cpu_usage: cpu_usage, created_at: created_at }.to_json(*a)
  end
end
