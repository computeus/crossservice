# Holds the Process Data gathered from server

class ServerProcessDatum < ActiveRecord::Base
  include LastDataAt

  belongs_to :server, inverse_of: :process_data

  has_many :process_data, inverse_of: :server_process_datum

  validates :server, presence: true
  validates :process_count, presence: true

  def to_json(*a)
    { server: server.hostname, process_count: process_count, created_at: created_at }.to_json(*a)
  end

  # Saves the given process data hash array to database
  # process_array = [{'process_name' => 'process_1', 'process_cpu_usage' => '10'}, {'process_name' => 'process_2', 'process_cpu_usage' => '20'}]
  def save_process_data(process_array)
    process_array.each do |process|
      process_data.create(process_name: process['process_name'], process_cpu_usage: process['process_cpu_usage'])
    end
  end
end
