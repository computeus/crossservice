class Server < ActiveRecord::Base
  validates :hostname, presence: true
  validates :user, presence: true

  belongs_to :user, inverse_of: :servers

  has_many :cpu_data, class_name: :ServerCpuDatum, inverse_of: :server
  has_many :disk_data, class_name: :ServerDiskDatum, inverse_of: :server
  has_many :process_data, class_name: :ServerProcessDatum, inverse_of: :server

  def to_json(*a)
    { 'hostname' => hostname }.to_json(*a)
  end
end
