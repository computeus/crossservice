class ServerDiskDatum < ActiveRecord::Base
  include LastDataAt

  belongs_to :server, inverse_of: :disk_data

  validates :server, presence: true
  validates :disk_usage, presence: true

  def to_json(*a)
    { server: server.hostname, disk_usage: disk_usage, created_at: created_at }.to_json(*a)
  end
end
