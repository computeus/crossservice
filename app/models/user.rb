class User < ActiveRecord::Base
  validates :api_token, presence: true

  has_many :servers, inverse_of: :user
end
