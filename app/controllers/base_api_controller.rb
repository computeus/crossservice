class BaseApiController < ApplicationController
  before_filter :parse_request, :authenticate_user_from_token!

  private
  # Sets the @user variable from api_token
  def authenticate_user_from_token!
    if !@json['api_token']
      render_unauthorized
    elsif !@user = User.find_by(api_token: @json['api_token'])
      render_unauthorized
    end
  end

  # Checks if the request is json
  def parse_request
    @json = JSON.parse(request.body.read) rescue render_unauthorized
  end

  # Error renderer methods
  def render_unauthorized
    render_status(:unauthorized)
  end

  def render_bad_request
    render_status(:bad_request)
  end

  def render_conflict
    render_status(:conflict)
  end

  def render_status(status)
    render nothing: true, status: status
  end

  # Reders bad_request if conditions not met
  def check_parameters(conditions)
    unless conditions
      render_bad_request
    end
  end
end
