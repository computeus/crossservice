class ApiServersController < BaseApiController
  before_filter :check_server_parameters, :server_exists?, only: :create
  before_filter :check_cpu_datum_parameters, only: :store_cpu
  before_filter :check_disk_datum_parameters, only: :store_disk
  before_filter :check_process_datum_parameters, only: :store_process
  before_filter :find_server, only: [:store_cpu, :store_disk, :store_process]

  # Creates a server for the authenticated user
  def create
    @server = @user.servers.new(hostname: @json['server']['hostname'])

    if @server.save
      render json: @server
    else
      render_bad_request
    end
  end

  # Creates a cpu data for the @server
  def store_cpu
    @cpu_datum = @server.cpu_data.new(cpu_usage: @json['cpu_data']['cpu_usage'])

    if @cpu_datum.save
      render json: @cpu_datum
    else
      render_bad_request
    end
  end

  # Creates a disk data for the @server
  def store_disk
    @disk_datum = @server.disk_data.new(disk_usage: @json['disk_data']['disk_usage'])

    if @disk_datum.save
      render json: @disk_datum
    else
      render_bad_request
    end
  end

  # Creates a process data for the @server
  def store_process
    @process_datum = @server.process_data.new(process_count: @json['process_data']['processes'].count)

    if @process_datum.save
      @process_datum.save_process_data(@json['process_data']['processes'])
      render json: @process_datum
    end
  end

  private

  def check_server_parameters
    unless @json.has_key?('server') && @json['server'].respond_to?(:[]) && @json['server']['hostname']
      render_bad_request
    end
  end

  def server_exists?
    if @user.servers.find_by(hostname: @json['server']['hostname'])
      render_conflict
    end
  end

  def find_server
    if !@server = @user.servers.find_by(hostname: params[:hostname])
      render_bad_request
    end
  end

  # Checks if the required cpu data parameters exists
  def check_cpu_datum_parameters
    check_parameters(@json.has_key?('cpu_data') && @json['cpu_data'].respond_to?(:[]) && @json['cpu_data']['cpu_usage'])
  end

  # Checks if the required disk data parameters exists
  def check_disk_datum_parameters
    check_parameters(@json.has_key?('disk_data') && @json['disk_data'].respond_to?(:[]) && @json['disk_data']['disk_usage'])
  end

  # Checks if the required process data parameters exists
  def check_process_datum_parameters
    check_parameters(@json.has_key?('process_data') && @json['process_data'].respond_to?(:[]) && @json['process_data']['processes'])
  end
end
