FactoryGirl.define do
  factory :process_datum do
    process_cpu_usage { rand(100) }
    process_name { Faker::Name.name }
    server_process_datum
  end
end