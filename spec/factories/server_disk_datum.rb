FactoryGirl.define do
  factory :server_disk_datum do
    disk_usage { rand(100) }
    server
  end
end