require 'rails_helper'

RSpec.describe ApiServersController, type: :controller do
  let(:user) { FactoryGirl.create :user }
  let(:server) { FactoryGirl.create :server, user: user }

  describe "POST #create" do
    it "returns http success" do
      post :create, { api_token: 'asdasdasd', server: { hostname: 'hostname2' } }.to_json
      expect(response).to have_http_status(:unauthorized)
    end

    it "returns http success" do
      post :create, { api_toen: 'asdasdasd', server: { hostname: 'hostname2' } }.to_json
      expect(response).to have_http_status(:unauthorized)
    end

    it "returns http success" do
      post :create, { api_token: user.api_token, server: { hostname: 'hostname2' } }.to_json
      expect(response).to have_http_status(:success)
    end

    it "returns bad request" do
      post :create, { api_token: user.api_token, server: { hostname: '' } }.to_json
      expect(response).to have_http_status(:bad_request)
    end

    it "returns bad request" do
      post :create, { api_token: user.api_token, server: { hostame: 'asdasdasd' } }.to_json
      expect(response).to have_http_status(:bad_request)
    end

    it "returns conflict" do
      FactoryGirl.create :server, user: user, hostname: 'hostname2'
      post :create, { api_token: user.api_token, server: { hostname: 'hostname2' } }.to_json
      expect(response).to have_http_status(:conflict)
    end
  end

  describe "POST #store_cpu" do
    it "returns http success" do
      post :store_cpu, { api_token: user.api_token, cpu_data: { cpu_usage: 22 } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:success)
    end

    it "returns bad request" do
      post :store_cpu, { api_token: user.api_token, cpu_data: { cpu_usage: '' } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:bad_request)
    end

    it "returns bad request" do
      post :store_cpu, { api_token: user.api_token, cpu_data: { cpu_usge: 22 } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:bad_request)
    end
  end

  describe "POST #store_disk" do
    it "returns http success" do
      post :store_disk, { api_token: user.api_token, disk_data: { disk_usage: 22 } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:success)
    end

    it "returns bad request" do
      post :store_disk, { api_token: user.api_token, disk_data: { disk_usage: '' } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:bad_request)
    end

    it "returns bad request" do
      post :store_disk, { api_token: user.api_token, disk_data: { disk_usage: '' } }.to_json, { hostname: 'asd' }
      expect(response).to have_http_status(:bad_request)
    end
  end

  describe "POST #store_process" do
    it "returns http success" do
      post :store_process, { api_token: user.api_token, process_data: { processes: [FactoryGirl.create(:process_datum)] } }.to_json, { hostname: server.hostname }
      expect(response).to have_http_status(:success)
    end
  end
end
