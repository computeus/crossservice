require 'rails_helper'

RSpec.describe ServerProcessDatum, type: :model do
  let(:server_process_datum) { FactoryGirl.create :server_process_datum }
  it { should validate_presence_of :server }
  it { should validate_presence_of :process_count }

  describe "#to_json" do
    it "should return server_name, process_count and created at attributes" do
      attributes = JSON.parse(server_process_datum.to_json)
      attributes.count.should eq(3)
      attributes.has_key?('server').should eq(true)
      attributes.has_key?('process_count').should eq(true)
      attributes.has_key?('created_at').should eq(true)
    end
  end

  describe "#save_process_data" do
    it "should save related process data" do
      server_process_datum.save_process_data([{'process_cpu_usage' => 50, 'process_name' => 'ali'}, {'process_cpu_usage' => 50, 'process_name' => 'veli'}])
      server_process_datum.process_data.count.should eq(2)
    end
  end
end
