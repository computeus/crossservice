require 'rails_helper'

RSpec.describe ServerCpuDatum, type: :model do
  let(:server_cpu_datum) { FactoryGirl.create :server_cpu_datum }
  it { should validate_presence_of :server }
  it { should validate_presence_of :cpu_usage }

  describe "#to_json" do
    it "should return server_name, cpu_usage and created at attributes" do
      attributes = JSON.parse(server_cpu_datum.to_json)
      attributes.count.should eq(3)
      attributes.has_key?('server').should eq(true)
      attributes.has_key?('cpu_usage').should eq(true)
      attributes.has_key?('created_at').should eq(true)
    end
  end
end
