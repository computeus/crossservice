require 'rails_helper'

RSpec.describe Server, type: :model do
  let(:server) { FactoryGirl.create :server }
  it { should validate_presence_of :hostname }
  it { should validate_presence_of :user }

  describe "#to_json" do
    it "should return hostname attributes" do
      attributes = JSON.parse(server.to_json)
      attributes.count.should eq(1)
      attributes.has_key?('hostname').should eq(true)
    end
  end
end