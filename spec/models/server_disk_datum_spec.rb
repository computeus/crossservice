require 'rails_helper'

RSpec.describe ServerDiskDatum, type: :model do
  let(:server_disk_datum) { FactoryGirl.create :server_disk_datum }
  it { should validate_presence_of :server }
  it { should validate_presence_of :disk_usage }

  describe "#to_json" do
    it "should return server_name, disk_usage and created at attributes" do
      attributes = JSON.parse(server_disk_datum.to_json)
      attributes.count.should eq(3)
      attributes.has_key?('server').should eq(true)
      attributes.has_key?('disk_usage').should eq(true)
      attributes.has_key?('created_at').should eq(true)
    end
  end
end
