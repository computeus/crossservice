require 'rails_helper'

RSpec.describe ProcessDatum, type: :model do
  let(:process_datum) { FactoryGirl.create :process_datum }

  it { should validate_presence_of :server_process_datum }
  it { should validate_presence_of :process_cpu_usage }
  it { should validate_presence_of :process_name }

  describe "#to_json" do
    it "should return server_name, cpu_usage, created at and process_name attributes" do
      attributes = JSON.parse(process_datum.to_json)
      attributes.count.should eq(4)
      attributes.has_key?('server').should eq(true)
      attributes.has_key?('cpu_usage').should eq(true)
      attributes.has_key?('created_at').should eq(true)
      attributes.has_key?('process_name').should eq(true)
    end
  end
end
