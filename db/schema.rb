# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160116183202) do

  create_table "process_data", force: :cascade do |t|
    t.integer  "server_process_datum_id", limit: 4
    t.integer  "process_cpu_usage",       limit: 4
    t.string   "process_name",            limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "process_data", ["process_cpu_usage"], name: "index_process_data_on_process_cpu_usage", using: :btree
  add_index "process_data", ["server_process_datum_id"], name: "index_process_data_on_server_process_datum_id", using: :btree

  create_table "server_cpu_data", force: :cascade do |t|
    t.integer  "server_id",  limit: 4
    t.integer  "cpu_usage",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "server_cpu_data", ["cpu_usage"], name: "index_server_cpu_data_on_cpu_usage", using: :btree
  add_index "server_cpu_data", ["server_id"], name: "index_server_cpu_data_on_server_id", using: :btree

  create_table "server_disk_data", force: :cascade do |t|
    t.integer  "server_id",  limit: 4
    t.integer  "disk_usage", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "server_disk_data", ["disk_usage"], name: "index_server_disk_data_on_disk_usage", using: :btree
  add_index "server_disk_data", ["server_id"], name: "index_server_disk_data_on_server_id", using: :btree

  create_table "server_process_data", force: :cascade do |t|
    t.integer  "server_id",     limit: 4
    t.integer  "process_count", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "server_process_data", ["process_count"], name: "index_server_process_data_on_process_count", using: :btree
  add_index "server_process_data", ["server_id"], name: "index_server_process_data_on_server_id", using: :btree

  create_table "servers", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.string   "hostname",          limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "custom_name",       limit: 255
    t.datetime "last_data_sent_at"
  end

  add_index "servers", ["hostname"], name: "index_servers_on_hostname", using: :btree
  add_index "servers", ["last_data_sent_at"], name: "index_servers_on_last_data_sent_at", using: :btree
  add_index "servers", ["user_id"], name: "index_servers_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "api_token",              limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
  end

  add_index "users", ["api_token"], name: "index_users_on_api_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
